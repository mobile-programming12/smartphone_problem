import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: 200, maxWidth: 700
              ),
              child: ListTile(
                    leading: const Icon(Icons.notifications),
                    title: const Text('Push notifications'),
                    trailing: Switch(value: true, onChanged: (_) {}),
                  ),
            ),
          )
          // Column(
          //   children:<Widget> [
          //     ConstrainedBox(
          //       constraints: BoxConstraints.expand(width: 100),
          //       child: ListTile(
          //         leading: const Icon(Icons.notifications),
          //         title: const Text('Push notifications'),
          //         trailing: Switch(value: true, onChanged: (_) {}),
          //         //trailing: Switch(value: true, onChanged: (_) {}),
          //       ),
          //     ),
          //   ],
          // )


          // LayoutBuilder(builder: (context,constraints){
          //   return ListTile(
          //     leading: const Icon(Icons.notifications),
          //     title: const Text('Push notifications'),
          //     trailing: Switch(value: true, onChanged: (_) {}),
          //   );
          // },

          // Center(
          //   ConstrainedBox(
          //     constraints: const BoxConstraints(
          //         minHeight: 50,
          //         minWidth: 50,
          //         maxHeight: 80,
          //         maxWidth: 80
          //     ),
          //     child: ListTile(
          //           leading: const Icon(Icons.notifications),
          //           title: const Text('Push notifications'),
          //           trailing: Switch(value: true, onChanged: (_) {}),
          //           //trailing: Switch(value: true, onChanged: (_) {}),
          //         ),
          //   ),
          // )
          // ConstrainedBox(
          //     constraints: const BoxConstraints(maxWidth: 100),
          //   child: ListTile(
          //     leading: const Icon(Icons.notifications),
          //     title: const Text('Push notifications'),
          //     trailing: Switch(value: true, onChanged: (_) {}),
          //     //trailing: Switch(value: true, onChanged: (_) {}),
          //   ),
          // ),

        ],
      ),
    );
  }
}