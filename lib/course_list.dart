import 'package:flutter/material.dart';

import 'data/course.dart';
import 'course_view.dart';
import 'drawer.dart';
import 'app_layout.dart';
import 'app_menu.dart';
import 'courses.dart';

class CourseList extends StatelessWidget {
  const CourseList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final smallerDevice = width < AppLayout.widthFirstBreakpoint;

    // final courseList = Padding(
    //     padding: const EdgeInsets.all(8.0),
    //     child: ListView.separated(
    //       itemBuilder: (_, index) =>
    //           CourseView(course: Course.shortList[index]),
    //       separatorBuilder: (_, __) => const SizedBox(height: 15),
    //       itemCount: Course.shortList.length,
    //     ),
    //   );

    const coursesGrid = Courses();


    // final courseWrap = SingleChildScrollView(
    //   child: Wrap(
    //     children: Course.shortList.map((coures)=> Container(
    //       constraints: const BoxConstraints(
    //         maxWidth: AppLayout.widthFirstBreakpoint
    //       ),child: CourseView(course: coures,),
    //     )).toList()
    //   ),
    // );

    // final courseGrid = GridView.extent(
    //    maxCrossAxisExtent:  450.0,
    //   childAspectRatio: 0.8,
    //   children: Course.shortList.map((course) => CourseView(course: course)
    //   ).toList(),
    // );


    // final courseList = Padding(
    //     padding: const EdgeInsets.all(8.0),
    //   child: courseGrid,
    //   // child: courseWrap,
    // );

    final courseList = Padding(
      padding: const EdgeInsets.all(8.0),
      child: coursesGrid,

    );
    return Scaffold(
      backgroundColor: Colors.green[900],
      appBar: AppBar(
        title: const Text('Not a responsive app'),
      ),
      // drawer: AppDrawer(),
      drawer: smallerDevice ? const AppDrawer() : null,
      // body: Padding(
      //   padding: const EdgeInsets.all(8.0),
      //   child: ListView.separated(
      //     itemBuilder: (_, index) =>
      //         CourseView(course: Course.shortList[index]),
      //     separatorBuilder: (_, __) => const SizedBox(height: 15),
      //     itemCount: Course.shortList.length,
      //   ),
      // ),
      body: smallerDevice 
          ? courseList 
          : Row(
            children: [
              const Expanded(
                flex: 1,
                  child: AppMenu()
              ),
              Expanded(
                  flex: 6,
                  child: courseList
              )
            ],
      ),
    );
  }
}