import 'package:flutter/material.dart';

class FlutterIsLove extends StatelessWidget {
  final double size;

  const FlutterIsLove({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Padding(
        padding: EdgeInsets.symmetric(
        horizontal: screenWidth * 0.1,
        vertical: screenHeight * 0.1,
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        LayoutBuilder(builder: (context,constraints ){
          final wideEnough = constraints.maxWidth;
          if (wideEnough > 350){
             return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlutterLogo(size: size),
                Icon(Icons.favorite, color: Colors.red, size: size),
              ]
              );
          } else {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlutterLogo(size: size),
              Icon(Icons.favorite, color: Colors.red, size: size),
            ]
            );
          }
        })
      ],
    )
    );
  }
}